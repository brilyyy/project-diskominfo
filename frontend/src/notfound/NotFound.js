import React from 'react'

const NotFound = () => {
    return(
        <div className='bg-gradient-to-tl from-blue-400 to-blue-200 w-full h-screen flex justify-center items-center'>
            <h1 className='text-6xl font-mono text-white antialiased'>404 | Not Found</h1>
        </div>
    )
}

export default NotFound