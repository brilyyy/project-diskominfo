<?php

namespace App\Http\Controllers;

use App\Models\KrawanganDetail;
use Illuminate\Http\Request;

class KrawanganDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KrawanganDetail  $krawanganDetail
     * @return \Illuminate\Http\Response
     */
    public function show(KrawanganDetail $krawanganDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KrawanganDetail  $krawanganDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(KrawanganDetail $krawanganDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KrawanganDetail  $krawanganDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KrawanganDetail $krawanganDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KrawanganDetail  $krawanganDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(KrawanganDetail $krawanganDetail)
    {
        //
    }
}
